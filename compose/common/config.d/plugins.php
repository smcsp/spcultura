<?php
return [
    'plugins' => [
        'EvaluationMethodTechnical' => ['namespace' => 'EvaluationMethodTechnical'],
        'EvaluationMethodSimple' => ['namespace' => 'EvaluationMethodSimple'],
        'EvaluationMethodDocumentary' => ['namespace' => 'EvaluationMethodDocumentary'],
        
        'MultipleLocalAuth' => ['namespace' => 'MultipleLocalAuth'],
        'AldirBlanc' => [
            'namespace' => 'AldirBlanc',
            'config' => [
                'project_id' => env('AB_INCISO2_PROJECT_ID',4786),
                'inciso1_enabled' => false,
                'inciso2_enabled' => false,
                'msg_inciso2_disabled' => env('AB_INCISO2_DISABLE_MESSAGE','O prazo para a solicitação deste benefício já foi encerrada.'),

                'inciso3_enabled' => false,
                'msg_inciso2_disabled' => 'As inscrições foram encerradas no dia 16/10/2020',
                'privacidade_termos_condicoes' => env('AB_PRIVACIDADE_TERMOS','https://mapacultural.pa.gov.br/files/subsite/2/termos-e-politica.pdf'),
                'link_suporte' => env('AB_LINK_SUPORTE','https://sp156.prefeitura.sp.gov.br/portal/busca?termo=lei+aldir+blanc&pagina=1'),          
                'texto_categoria_espaco-formalizado' => env('AB_TXT_CAT_ESPACO_FORMALIZADO', '<strong>Entidade, empresa ou cooperativa do setor cultural com inscrição em CNPJ</strong> para espaço do tipo <strong>Espaço físico próprio, alugado, público cedido em comodato, emprestado ou de uso compartilhado</strong>.' ),
                'texto_categoria_espaco-nao-formalizado' => env('AB_TXT_CAT_ESPACO_NAO_FORMALIZADO', '<strong>Espaço artístico e cultural mantido por coletivo ou grupo cultural (sem CNPJ) ou por pessoa física (CPF)</strong> para espaço do tipo <strong>Espaço físico próprio, alugado, público cedido em comodato, emprestado ou de uso compartilhado</strong>.' ),
                'texto_categoria_coletivo-formalizado' => env('AB_TXT_CAT_COLETIVO_FORMALIZADO', '<strong>Entidade, empresa ou cooperativa do setor cultural com inscrição em CNPJ</strong> para espaço do tipo <strong>Espaço público (praça, rua, escola, quadra ou prédio custeado pelo poder público)</strong>.' ),
                'texto_categoria_coletivo-nao-formalizado' => env('AB_TXT_CAT_COLETIVO_NAO_FORMALIZADO', '<strong>Espaço artístico e cultural mantido por coletivo ou grupo cultural (sem CNPJ) ou por pessoa física (CPF)</strong> para espaço do tipo <strong>Espaço público (praça, rua, escola, quadra ou prédio custeado pelo poder público)</strong>.' ),
                'texto_cadastro_espaco'  => env('AB_TXT_CADASTRO_ESPACO', 'Espaço físico próprio, alugado, público cedido em comodato, emprestado ou de uso compartilhado.'),
                'texto_cadastro_coletivo'  => env('AB_TXT_CADASTRO_COLETIVO', 'Espaço público (praça, rua, escola, quadra ou prédio custeado pelo poder público).'),
                'texto_cadastro_cpf'  => env('AB_TXT_CADASTRO_CPF', 'Coletivo ou grupo cultural (sem CNPJ). Pessoa física (CPF) que mantêm espaço artístico'),
                'texto_cadastro_cnpj'  => env('AB_TXT_CADASTRO_CNPJ', 'Entidade, empresa ou cooperativa do setor cultural com inscrição em CNPJ.'),
                'inciso2' => (array) json_decode(env('AB_INCISO2_CITIES', '[]')),
                'texto_home'=> env('AB_TEXTO_HOME','A Lei de Emergência Cultural Aldir Blanc (Lei nº 14.017/2020) estabelece mecanismos e critérios para garantir apoio às trabalhadoras e trabalhadores da cultura e à manutenção de territórios/espaços culturais com atividades interrompidas por força da pandemia causada pelo novo coronavírus. Os repasses da União ao município de São Paulo pela Lei Aldir Blanc totalizam R$ 70.854.049,80, que estão sendo investidos no pagamento do subsídio a territórios e espaços culturais, além de editais de premiação contemplando as mais diversas linguagens artísticas. Essas ações emergenciais, que representam um investimento de R$ 70.854.049,80, se somam a uma série de atividades que integram o plano municipal de incentivo à cultura e ao setor artístico lançadas desde março, quando iniciou-se o distanciamento social imposto pelo combate ao Covid-19. Desde o mês de julho, o Grupo de Trabalho de Acompanhamento e Fiscalização da Lei Aldir Blanc, criado por meio do Decreto Municipal nº 59.580 e formado por representantes da sociedade civil, da Prefeitura e da Câmara Municipal, vem se reunindo com o objetivo de pensar nas melhores soluções para a destinação da verba, além de acompanhar, orientar e fiscalizar a execução das ações previstas pela lei no município. Buscando atingir o maior número possível de beneficiários, 30% dos recursos estão sendo destinados para o atendimento aos territórios e espaços culturais e 70% para os editais de premiação. Essa divisão levou em conta o levantamento realizado em 2018 pelo Dieese, que identificou a existência de 1.044 estabelecimentos em Atividades da Economia Criativa no município de São Paulo. Visite o <a href="http://cultura.prefeitura.sp.gov.br"> site</a> da Secretaria Municipal de Cultura para acessar a portaria, os editais e outras informações sobre a Lei Aldir Blanc na cidade de São Paulo. <b>ATENÇÃO: Territórios/espaços culturais pertencentes ao município de São Paulo deverão solicitar o subsídio EXCLUSIVAMENTE junto à Secretaria Municipal de Cultura da Cidade de São Paulo, pela plataforma SP Cultura. Não serão válidos cadastros realizados pela plataforma do Governo do Estado de São Paulo.</b>'),
                'botao_home'=> env('AB_BOTAO_HOME','Solicite o auxílio ou inscreva-se nos editais'),
                'titulo_home'=> env('AB_TITULO_HOME','Lei Aldir Blanc na cidade de São Paulo'),
                'inciso2_opportunity_ids' => ['São Paulo' => 4787],
                'inciso3_opportunity_ids' => [
                    'Inciso III Sao Paulo 4788' => 4788,
                    'Inciso III Sao Paulo 4789' => 4789,
                    'Inciso III Sao Paulo 4790' => 4790,
                    'Inciso III Sao Paulo 4791' => 4791,
                    'Inciso III Sao Paulo 4792' => 4792,
                    'Inciso III Sao Paulo 4793' => 4793,
                    'Inciso III Sao Paulo 4794' => 4794,
                    'Inciso III Sao Paulo 4795' => 4795,
                ],

                'relatorios.inciso3.registrationFields' => (object)[
                    'Subprefeitura' => 'Subprefeitura',
                    'Tipo de Proponente' => 'Tipo de Proponente',
                    'Gênero' => 'Gênero'
                ],

                'relatorios.inciso2.registrationFields' => (object)[
                    'Expressão cultural desenvolvida' => [1083],
                    'Beneficiário do subsídio' => [1036],
                    'Beneficiário do subsídio (organização formal)' => [1040],
                    'Responsável - Subprefeitura' => [1041],
                    'Responsável - Sexo' => [994],
                    'Responsável - Grau de escolaridade' => [1068],
                    'Espaço/Beneficiário - Subprefeitura' => [1048,1070]
                ]
            ]
        ],
        'AldirBlancDataprev' => [
            'namespace' => 'AldirBlancDataprev',
            'config' => [
                'consolidacao_requer_validacoes' => ['financeiro']
            ],
        ],

        'Recursos' => ['namespace' => 'AldirBlancValidadorRecurso'],
        
        'RegistrationPayments' => [ 'namespace' => 'RegistrationPayments' ],

        'Recursos' => ['namespace' => 'AldirBlancValidadorRecurso'],
        
        'Financeiro' => [
            'namespace' => 'AldirBlancValidadorFinanceiro',
            'config' => [
                'exportador_requer_validacao' => ['dataprev'],
                'consolidacao_requer_homologacao' => false,
                'consolidacao_requer_validacoes' => []
            ],
        ],

        'SMCSP' => [
            'namespace' => 'AldirBlancValidador',
            'config' => [
                // slug utilizado como id do controller e identificador do validador
                'slug' => 'smcsp',

                // nome apresentado na interface
                'name' => 'SMCSP',

                'forcar_resultado' => true,

                'consolidacao_requer_homologacao' => false,

                // indica que só deve exportar as inscrições já homologadas
                'exportador_requer_homologacao' => true,

                // indica que a exportação não requer nenhuma validação
                'exportador_requer_validacao' => [],

                // indica que só deve consolidar o resultado se a inscrição
                // já tiver sido processada pelo Dataprev
                'consolidacao_requer_validacoes' => [],

                'inciso1' => [
                    'AVALIACAO' => function ($registration, $key) {
                        return $registration->consolidatedResult;
                    }
                ],
            ]
        ],

        'PreDataprev' => [
            'namespace' => 'AldirBlancValidador',
            'config' => [
                // slug utilizado como id do controller e identificador do validador
                'slug' => 'pre_dataprev',

                // nome apresentado na interface
                'name' => 'Pré-processamento Dataprev',

                'forcar_resultado' => true,

                'consolidacao_requer_homologacao' => false,

                // invalidada a exportação pq não faz sentido
                'exportador_requer_validacao' => ['nao-exportar'],

                'consolidacao_requer_validacoes' => [],

                'inciso1' => [],
            ]
        ],

        'MapasBlame' => ['namespace' => 'MapasBlame'],

    ],
];
