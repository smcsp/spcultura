<?php
return [
    'app.offline' => is_dir(BASE_PATH . 'em-manutencao'),
    'app.offlineUrl' => '/em-manutencao',
    'app.offlineBypassFunction' => function() {
        $senha = $_GET['online'] ?? '';
        
        if ($senha === 'aldir.blanc.sp') {
            $_SESSION['online'] = true;
        }

        return $_SESSION['online'] ?? false;
    }
];  