<?php
$this->jsObject['registrationId'] = $registration_id;
?>
<section class="termos">
    <p class="termos--summay"><?php \MapasCulturais\i::_e("De acordo com os critérios da LEI Nº 14.017 com sanção presidencial em 29 DE JUNHO DE 2020. Para ter acesso ao Auxílio Emergencial Cultural, você deve cumprir os seguintes requisitos:"); ?> </p>

    <h2><?php \MapasCulturais\i::_e("Termos e Condições Inciso II"); ?></h2>

    <div class="termos--list">
        <div class="term">
            <span class="term--box"></span>
            <label class="term--label">
                <input type="checkbox" class="term--input" />
                <span class="termos--text">
                    Declaro que o(a) espaço artístico e cultural, micro ou pequena empresa cultural, organização cultural comunitária, cooperativa cultural ou instituição cultural ao qual represento tem finalidade cultural e teve suas atividades interrompidas em decorrência da pandemia da covid 19, conforme dispõe o inciso II do artigo 2º da Lei 14.017/2020;

                </span>
            </label>
        </div>
        <div class="term">
            <span class="term--box"></span>
            <label class="term--label">
                <input type="checkbox" class="term--input" />
                <span class="termos--text">
                    Declaro que o espaço artístico e cultural NÃO foi criado ou está vinculado à administração pública de qualquer esfera, conforme vedação prevista no § Único do Art. 8º da Lei 14.017/2020;
                </span>
            </label>
        </div>
        <div class="term">
            <span class="term--box"></span>
            <label class="term--label">
                <input type="checkbox" class="term--input" />
                <span class="termos--text">
                    Declaro que o espaço artístico e cultural NÃO está vinculado às fundações, institutos ou instituições criados ou mantidos por grupos de empresas, conforme vedação prevista no § Único do Art. 8º da Lei 14.017/2020;
                </span>
            </label>
        </div>
        <div class="term">
            <span class="term--box"></span>
            <label class="term--label">
                <input type="checkbox" class="term--input" />
                <span class="termos--text">
                    Declaro que o espaço artístico e cultural NÃO é gerido pelos serviços sociais do Sistema S (Sescoop, Sesi, Senai, Sesc, Senac, Sest, Senat e Senar), conforme vedação prevista no § Único do Art. 8º da Lei 14.017/2020;
                </span>
            </label>
        </div>
        <div class="term">
            <span class="term--box"></span>
            <label class="term--label">
                <input type="checkbox" class="term--input" />
                <span class="termos--text">
                    Declaro que estou solicitando apenas este subsídio mensal, em todo território nacional, e que não irei requerer esse mesmo benefício para outro espaço artístico e cultural ao qual sou responsável pela gestão, pois estou ciente da vedação de recebimento cumulativo prevista no § 3º do artigo 7º da Lei 14.017/2020;

                </span>
            </label>
        </div>
        <div class="term">
            <span class="term--box"></span>
            <label class="term--label">
                <input type="checkbox" class="term--input" />
                <span class="termos--text">
                Declaro que estou ciente de que devo apresentar ao ente pagador do benefício, previsto na Lei 14.017/2020, a prestação de contas em até 120 (cento e vinte) dias após o recebimento dos recursos, nos termos do art. 10º da referida Lei;
                </span>
            </label>
        </div>
        <div class="term">
            <span class="term--box"></span>
            <label class="term--label">
                <input type="checkbox" class="term--input" />
                <span class="termos--text">
                Declaro que o espaço artístico e cultural está sediado e exerce atividades culturais no Município de São Paulo conforme previsto no artigo 3º do Decreto Municipal Nº 59.796, de 28 de setembro de 2020;  
                </span>
            </label>
        </div>
        <div class="term">
            <span class="term--box"></span>
            <label class="term--label">
                <input type="checkbox" class="term--input" />
                <span class="termos--text">
                Declaro que o espaço artístico e cultural obriga-se a cumprir a oferta de atividades ou bens em contrapartida, após o reinício de suas atividades, conforme conforme previsto no artigo 9º da Lei Federal nº 14.017/20; 
                </span>
            </label>
        </div>
        <div class="term">
            <span class="term--box"></span>
            <label class="term--label">
                <input type="checkbox" class="term--input" />
                <span class="termos--text">
                Declaro que o espaço artístico e cultural existe a no mínimo 06 (seis) meses. Ou seja, anterior à data de 20/03/2020, quando publicado o Decreto Legislativo 06/2020 que instituiu o estado de calamidade pública. Conforme previsto nos  Art. 8º e 13 da portaria da SMC Nº 88 de 29 de setembro de 2020;

                </span>
            </label>
        </div>
        <div class="term">
            <span class="term--box"></span>
            <label class="term--label">
                <input type="checkbox" class="term--input" />
                <span class="termos--text">
                Declaro que li e que concordo com os termos previstos em edital e no decreto de regulamentação publicado pelo ente repassador dos recursos do subsídio mensal;

                </span>
            </label>
        </div>
        <div class="term">
            <span class="term--box"></span>
            <label class="term--label">
                <input type="checkbox" class="term--input" />
                <span class="termos--text">
                Declaro, sob as penas previstas na legislação, que as informações prestadas nesta declaração são verdadeiras, e que estou ciente das penalidades previstas no art. 299 do Decreto-Lei nº 2.848, de 7 de dezembro de 1940 do Código Penal, além da possibilidade responsabilização na esfera civil e administrativa.
                </span>
            </label>
        </div>

        <nav class="termos--nav-terms">
            <button class="btn btn-large btn-lab js-btn">  <?php \MapasCulturais\i::_e("Continuar");?></button>
        </nav>

        <div id="modalAlert" class="modal">
            <!-- Modal content -->
            <div class="modal-content">
                <h2 class="modal-content--title title-modal">Atenção!</h2>
                <span class="close">&times;</span>
                <p>
                    <?php \MapasCulturais\i::_e("Você precisa aceitar todos os termos para proseguir com a inscrição no auxílio emergencial da cultura.");?>
                </p>

                <button id="btn-close" class="btn "> OK </button>
            </div>
        </div>
</section>

<script>
    var span = document.getElementsByClassName("close")[0];
    var modal = document.getElementById("modalAlert");
    var btnClose = document.getElementById("btn-close");
    var btnProsseguir = document.querySelector(".js-btn");

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == btnProsseguir) {
            goToNextPage();
        } else {
            if (modal.style.display == 'flex') {
                modal.style.display = "none";
            }
        }

    }
    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }

    btnClose.onclick = function() {
        modal.style.display = "none";
    }

    function goToNextPage() {
        var checkboxes = document.querySelectorAll('input[type="checkbox"]');
        var checkboxesChecked = document.querySelectorAll('input[type="checkbox"]:checked');

        if (checkboxes.length === checkboxesChecked.length) {
            //redirect to next page
            document.location = MapasCulturais.createUrl('aldirblanc', 'aceitar_termos', [MapasCulturais.registrationId])
        } else {
            modal.style.display = "flex";
        }
    }
</script>