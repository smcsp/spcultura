<?php
namespace ConsolidadorInciso2;
use MapasCulturais\App;

class Plugin extends \MapasCulturais\Plugin {
    function __construct(array $config = [])
    {
        $config += [
            'interval' => 10,
            'opportunity_id' => null,
            'dataprev_user_id' => null
        ];

        parent::__construct($config);
    }

    public function register() {
        // register metadata, taxonomies

    }

    public function _init() {
        $app = App::i();
        $cache_id = __METHOD__ . ':' . $this->_config['opportunity_id'];

        if(!$app->cache->contains($cache_id)) {
            $app->cache->save($cache_id, true, intval($this->_config['interval']));
            $this->runUpdates();
        }
    }

    public function runUpdates() {
        $app = App::i();
        $app->log->debug(__METHOD__);

        $conn = $app->em->getConnection();

        $queries = [

            "UPDATE registration 
                SET consolidated_result = '10' 
                WHERE 
                    opportunity_id = :opportunity_id AND
                    id IN (SELECT registration_id FROM registration_evaluation WHERE status = 1 AND result = '10')",

            "UPDATE registration 
                SET consolidated_result = '8' 
                WHERE 
                    opportunity_id = :opportunity_id AND
                    id IN (SELECT registration_id FROM registration_evaluation WHERE status = 1 AND result = '8')",

            "UPDATE registration 
                SET consolidated_result = '3' 
                WHERE 
                    opportunity_id = :opportunity_id AND
                    id IN (SELECT registration_id FROM registration_evaluation WHERE status = 1 AND result = '3')",

            "UPDATE registration 
                SET consolidated_result = '2' 
                WHERE 
                    opportunity_id = :opportunity_id AND
                    id IN (SELECT registration_id FROM registration_evaluation WHERE status = 1 AND result = '2')",
                    
            "UPDATE registration 
                SET consolidated_result = 'homologado' 
                WHERE 
                    opportunity_id = :opportunity_id AND
                    
                    -- contém avaliação como selecionado
                    id IN (SELECT registration_id FROM registration_evaluation WHERE result = '10') AND

                    -- não contém avaliação diferente de selecionado
                    id NOT IN (SELECT registration_id FROM registration_evaluation WHERE result <> '10') AND

                    -- não foi validado ou invalidado pelo dataprev
                    id NOT IN (SELECT registration_id FROM registration_evaluation WHERE user_id = :user_id)",

            "UPDATE registration 
                SET consolidated_result = 'homologado, validado por Dataprev' 
                WHERE 
                    opportunity_id = :opportunity_id AND
                    
                    -- contém avaliação como selecionado
                    id IN (SELECT registration_id FROM registration_evaluation WHERE result = '10') AND

                    -- não contém avaliação diferente de selecionado
                    id NOT IN (SELECT registration_id FROM registration_evaluation WHERE result <> '10' AND user_id <> :user_id) AND

                    -- foi validado pelo dataprev
                    id IN (SELECT registration_id FROM registration_evaluation WHERE user_id = :user_id AND result = '10')",

            "UPDATE registration 
                SET consolidated_result = 'homologado, invalidado por Dataprev' 
                WHERE 
                    opportunity_id = :opportunity_id AND
                    
                    -- contém avaliação como selecionado
                    id IN (SELECT registration_id FROM registration_evaluation WHERE result = '10') AND

                    -- não contém avaliação diferente de selecionado
                    id NOT IN (SELECT registration_id FROM registration_evaluation WHERE result <> '10' AND user_id <> :user_id) AND

                    -- foi invalidado pelo dataprev
                    id IN (SELECT registration_id FROM registration_evaluation WHERE user_id = :user_id AND result = '2')",


            "UPDATE registration 
                SET consolidated_result = 'validado por Dataprev, Não selecionada' 
                WHERE 
                    opportunity_id = :opportunity_id AND
                    
                    -- contém avaliação como não selecionada
                    id IN (SELECT registration_id FROM registration_evaluation WHERE result = '3') AND

                    -- foi validado pelo dataprev
                    id IN (SELECT registration_id FROM registration_evaluation WHERE user_id = :user_id AND result = '10')",

            "UPDATE registration 
                SET consolidated_result = 'validado por Dataprev, Suplente' 
                WHERE 
                    opportunity_id = :opportunity_id AND
                    
                    -- contém avaliação como não selecionada
                    id IN (SELECT registration_id FROM registration_evaluation WHERE result = '8') AND

                    -- foi validado pelo dataprev
                    id IN (SELECT registration_id FROM registration_evaluation WHERE user_id = :user_id AND result = '10')",
        ];

        foreach ($queries as $sql) {
            $conn->executeQuery($sql, [
                'user_id' => $this->_config['dataprev_user_id'],
                'opportunity_id' => $this->_config['opportunity_id'],
            ]);
        }
    }
}